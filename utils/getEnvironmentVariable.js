/**
 * Get environment variable type safe
 * @param {string} name
 * @returns {string}
 */
function getEnvironmentVariable(name) {
    const variable = process.env[name];
    if (variable === undefined) {
        throw new Error(`Environment variable ${name} is undefined`);
    }

    return variable;
}

exports.getEnvironmentVariable = getEnvironmentVariable;
