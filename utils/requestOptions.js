const {headers} = require("./headers");
const {getJWT} = require("./auth");

const requestOptions = async (method, body) => {
    const token = await getJWT();
    return {
        "headers": headers(token),
        "referrer": "https://console.aws.amazon.com/",
        "referrerPolicy": "strict-origin-when-cross-origin",
        body,
        method,
        "mode": "cors"
    }
}

module.exports = {
    requestOptions
};
