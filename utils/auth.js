const dotenv = require('dotenv');
const {getEnvironmentVariable} = require('./getEnvironmentVariable');
const {
    CognitoUserPool,
    AuthenticationDetails,
    CognitoUser
} = require('amazon-cognito-identity-js');
dotenv.config({path: '.env'});

const poolData = {
    UserPoolId: getEnvironmentVariable("COGNITO_USER_POOL_ID"),
    ClientId: getEnvironmentVariable("COGNITO_CLIENT_ID")
};

const userPool = new CognitoUserPool(poolData);

function getCognitoUser(userName) {
    const userParams = {
        Pool: userPool,
        Username: userName
    };

    return new CognitoUser(userParams);
}

async function getJWT() {
    const username = getEnvironmentVariable("USERNAME");
    return new Promise((resolve, reject) => {
        const authenticationDetails = new AuthenticationDetails({
            Username: username,
            Password: getEnvironmentVariable("PASSWORD")
        });
        getCognitoUser(username).authenticateUser(authenticationDetails, {
            onSuccess: result => {
                const token = result.getAccessToken().getJwtToken();
                resolve(token);
            },
            onFailure: err => {
                reject(err);
            }
        });
    });
}

exports.getJWT = getJWT;
