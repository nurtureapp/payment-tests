const headers = (AUTH_TOKEN) => {
   return {
       "accept": "application/json, text/plain, */*",
       "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,id;q=0.6",
       "authorization": AUTH_TOKEN,
       "cache-control": "no-cache",
       "content-type": "application/json",
       "pragma": "no-cache",
       "sec-ch-ua": "\"Google Chrome\";v=\"89\", \"Chromium\";v=\"89\", \";Not A Brand\";v=\"99\"",
       "sec-ch-ua-mobile": "?0",
       "sec-fetch-dest": "empty",
       "sec-fetch-mode": "cors",
       "sec-fetch-site": "cross-site",
       "x-amz-user-agent": "AWS-Console-AppSync/"
   }
}

module.exports = {
    headers
};
