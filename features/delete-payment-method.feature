Feature: Delete payment methods

  Scenario: Delete default payment method
    Given As a Parent
    And I create profile and add a credit card method
    When I delete the credit card
    Then I should have a correct delete method response

  Scenario: Delete default method when user have 2 payment methods
    Given I create profile and add a credit card method
    And I add a default payment method to existing profile
    When I delete DEFAULT credit card
    Then I should get an error response

