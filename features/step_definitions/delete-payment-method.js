const assert = require('assert');
const fetch = require('node-fetch');
const {When, Then} = require('@cucumber/cucumber');
const {requestOptions} = require("../../utils/requestOptions");
const {getEnvironmentVariable} = require("../../utils/getEnvironmentVariable");

When('I delete the credit card', async function() {
    const GRAPHQL_URL = getEnvironmentVariable("GRAPHQL_URL");
    const input = {
        profileId: this.profileId,
        accountId: this.accountId,
    };
    const query = `mutation deleteCardPointePaymentAccount($input: DeleteCardPointePaymentAccountInput!) {
      deleteCardPointePaymentAccount(inputData: $input)
    }`;
    const body = JSON.stringify({query, variables: {input}});

    try {
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);

        assert.strictEqual(200, res.status);
        this.res = await res.json();
    } catch (e) {
        console.log("Error in Delete Payment Account = ", e)
    }
});

When('I delete DEFAULT credit card', async function() {
    const GRAPHQL_URL = getEnvironmentVariable("GRAPHQL_URL");
    const input = {
        profileId: this.profileId,
        accountId: this.accountId_2,
    };
    const query = `mutation deleteCardPointePaymentAccount($input: DeleteCardPointePaymentAccountInput!) {
      deleteCardPointePaymentAccount(inputData: $input)
    }`;
    const body = JSON.stringify({query, variables: {input}});

    try {
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);

        assert.strictEqual(200, res.status);
        this.res = await res.json();
    } catch (e) {
        console.log("Error in Delete Payment Account = ", e)
    }
});

Then('I should have a correct delete method response', function() {
    assert.ok(this.res.data.deleteCardPointePaymentAccount);
});

Then('I should get an error response', function() {
    assert.ok(this.res.errors[0].errorType === 'Lambda:Unhandled');
    assert.ok(this.res.errors[0].message === 'You can not delete the default method, please select another one and try again');
});
