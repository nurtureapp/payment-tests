const {requestOptions} = require("../../utils/requestOptions");
const {getEnvironmentVariable} = require("../../utils/getEnvironmentVariable");
const assert = require('assert');
const fetch = require('node-fetch');
const {When, Then} = require('@cucumber/cucumber');
const faker = require('faker');

const GRAPHQL_URL = getEnvironmentVariable("GRAPHQL_URL");

When('I create profile and add a credit card method', {timeout: 10000}, async function () {
    const schoolId = getEnvironmentVariable("SCHOOL_ID");
    const token = getEnvironmentVariable("CREDIT_CARD_TOKEN_1");

    const input = {
        token,
        schoolId,
        name: faker.name.findName(),
        postal: faker.address.zipCode(),
        isDefault: true,
        address: faker.address.streetAddress(),
        address2: faker.address.secondaryAddress(),
        city: faker.address.cityName(),
        country: faker.address.countryCode(),
        expiry: "1025"
    };

    const query = `mutation createCardPointePaymentAccount($input: CreateCardPointePaymentAccountInput!) {
      createCardPointePaymentAccount(inputData: $input) {
        profileId
        accountId
      }
  }`;
    const body = JSON.stringify({query, variables: {input}});

    try {
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);
        assert.strictEqual(200, res.status);
        this.res = await res.json();

        this.profileId = this.res.data.createCardPointePaymentAccount.profileId;
        this.accountId = this.res.data.createCardPointePaymentAccount.accountId;
        this.schoolId = schoolId;
    } catch (e) {
        console.error(e)
        throw new Error(e)
    }
});

When('I add a credit card method to existing profile', {timeout: 6000}, async function () {
    const token = getEnvironmentVariable("CREDIT_CARD_TOKEN_1");

    const input = {
        profileId: this.profileId,
        region: faker.address.county(),
        schoolId: this.schoolId,
        name: faker.name.findName(),
        postal: faker.address.zipCode(),
        token,
        isDefault: false,
        address: faker.address.streetAddress(),
        address2: faker.address.secondaryAddress(),
        city: faker.address.cityName(),
        country: faker.address.countryCode(),
        expiry: "1025"
    };

    const query = `mutation upsertCardPointePaymentAccount($input: UpsertCardPointePaymentAccountInput!) {
      upsertCardPointePaymentAccount(inputData: $input) {
        profileId
        accountId
      }
  }`;
    const body = JSON.stringify({query, variables: {input}});
    try {
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);
        assert.strictEqual(200, res.status);

        this.res = await res.json();
        this.profileId = this.res.data.upsertCardPointePaymentAccount.profileId;
        this.accountId_2 = this.res.data.upsertCardPointePaymentAccount.accountId;
        this.account_2_isDefault = input.isDefault;
    } catch (e) {
        console.error(e)
        throw new Error(e)
    }
});

When('I add a default payment method to existing profile', {timeout: 6000}, async function () {
    const token = getEnvironmentVariable("CREDIT_CARD_TOKEN_2");

    const input = {
        token,
        profileId: this.profileId,
        region: faker.address.county(),
        schoolId: this.schoolId,
        name: faker.name.findName(),
        postal: faker.address.zipCode(),
        isDefault: true,
        address: faker.address.streetAddress(),
        address2: faker.address.secondaryAddress(),
        city: faker.address.cityName(),
        country: faker.address.countryCode(),
        expiry: "1025"
    };

    const query = `mutation upsertCardPointePaymentAccount($input: UpsertCardPointePaymentAccountInput!) {
      upsertCardPointePaymentAccount(inputData: $input) {
        profileId
        accountId
      }
  }`;
    const body = JSON.stringify({query, variables: {input}});
    try {
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);
        assert.strictEqual(200, res.status);

        this.res = await res.json();
        this.profileId = this.res.data.upsertCardPointePaymentAccount.profileId;
        this.accountId_2 = this.res.data.upsertCardPointePaymentAccount.accountId;
        this.account_2_isDefault = input.isDefault
    } catch (e) {
        console.error(e)
        throw new Error(e)
    }
});

Then('I should have a valid create account response', async function () {
    assert.ok(Number.isInteger(parseInt(this.profileId, 10)));
    assert.ok(Number.isInteger(parseInt(this.accountId, 10)));
});

When('I create profile and add bank account', {timeout: 6000}, async function () {
    const schoolId = getEnvironmentVariable("SCHOOL_ID");
    const token = getEnvironmentVariable("BANK_ACCOUNT_TOKEN_1");

    const input = {
        token,
        schoolId,
        name: faker.name.findName(),
        isDefault: true,
        accountType: faker.random.arrayElement(['ESAV', 'ECHK']),
        bankName: faker.finance.accountName(),
        bankaba: faker.internet.domainName(),
    };

    const query = `mutation createCardPointePaymentBankAccount($input: CreateCardPointePaymentBankAccountInput!) {
      createCardPointePaymentBankAccount(inputData: $input) {
        profileId
        accountId
      }
  }`;
    const body = JSON.stringify({query, variables: {input}});
    try {
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);
        assert.strictEqual(200, res.status);

        this.res = await res.json();

        this.bankAccountProfileId = this.res.data.createCardPointePaymentBankAccount.profileId;
        this.bankAccountId = this.res.data.createCardPointePaymentBankAccount.accountId;
        this.schoolId = schoolId;
    } catch (e) {
        console.error(e)
        throw new Error(e)
    }
});

When('I add a bank account to existing profile', {timeout: 6000}, async function () {
    const token = getEnvironmentVariable("BANK_ACCOUNT_TOKEN_2");

    const input = {
        token,
        profileId: this.bankAccountProfileId,
        schoolId: this.schoolId,
        name: faker.name.findName(),
        isDefault: false,
        accountType: faker.random.arrayElement(['ESAV', 'ECHK']),
        bankName: faker.finance.accountName(),
        bankaba: faker.internet.domainName(),
    };

    const query = `mutation upsertCardPointePaymentBankAccount($input: UpsertCardPointePaymentBankAccountInput!) {
      upsertCardPointePaymentBankAccount(inputData: $input) {
        profileId
        accountId
      }
  }`;
    const body = JSON.stringify({query, variables: {input}});
    try {
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);
        assert.strictEqual(200, res.status);

        this.res = await res.json();
        this.bankAccountProfileId = this.res.data.upsertCardPointePaymentBankAccount.profileId;
        this.bankAccountId_2 = this.res.data.upsertCardPointePaymentBankAccount.accountId;
        this.account_2_isDefault = input.isDefault
    } catch (e) {
        console.error(e)
        throw new Error(e)
    }
});

Then('I should have a valid create bank account response', async function () {
    assert.ok(Number.isInteger(parseInt(this.bankAccountProfileId, 10)));
    assert.ok(Number.isInteger(parseInt(this.bankAccountId, 10)));
});
