const {requestOptions} = require("../../utils/requestOptions");
const {getEnvironmentVariable} = require("../../utils/getEnvironmentVariable");
const assert = require('assert');
const fetch = require('node-fetch');
const {When, Then} = require('@cucumber/cucumber');
const faker = require('faker');
const GRAPHQL_URL = getEnvironmentVariable("GRAPHQL_URL");

When('I update a credit card method', async function () {
    const token = getEnvironmentVariable('CREDIT_CARD_TOKEN_1')

    const input = {
        token,
        profileId: this.profileId,
        accountId: this.accountId,
        schoolId: this.schoolId,
        name: faker.name.findName(),
        postal: faker.address.zipCode(),
        isDefault: faker.datatype.boolean(),
        address: faker.address.streetAddress(),
        address2: faker.address.secondaryAddress(),
        city: faker.address.cityName(),
        country: faker.address.countryCode(),
        expiry: "1025"
    };
    const query = `mutation upsertCardPointePaymentAccount($input: UpsertCardPointePaymentAccountInput!) {
      upsertCardPointePaymentAccount(inputData: $input) {
          profileId
          accountId
      }
    }`;

    try {
        const body = JSON.stringify({query, variables: {input}});
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);

        assert.strictEqual(200, res.status);
        this.res = await res.json();
    } catch (e) {
        console.log("Error in update credit card = ", e)
    }
});

Then('I should have a valid update creadit card response', function() {
    assert.ok(Number.isInteger(parseInt(this.res.data.upsertCardPointePaymentAccount.profileId, 10)));
    assert.ok(Number.isInteger(parseInt(this.res.data.upsertCardPointePaymentAccount.accountId, 10)));
    assert.ok(this.res.data.upsertCardPointePaymentAccount.accountId === this.accountId);
    assert.ok(this.res.data.upsertCardPointePaymentAccount.profileId === this.profileId);
});

When('I update a bank account method', async function () {
    const token = getEnvironmentVariable("BANK_ACCOUNT_TOKEN_3");

    const input = {
        token,
        profileId: this.bankAccountProfileId,
        accountId: this.bankAccountId,
        schoolId: this.schoolId,
        name: faker.name.findName(),
        isDefault: faker.datatype.boolean(),
        accountType: faker.random.arrayElement(['ESAV', 'ECHK']),
        bankName: faker.finance.accountName(),
        bankaba: faker.internet.domainName(),
    };
    const query = `mutation upsertCardPointePaymentBankAccount($input: UpsertCardPointePaymentBankAccountInput!) {
      upsertCardPointePaymentBankAccount(inputData: $input) {
          profileId
          accountId
      }
    }`;

    try {
        const body = JSON.stringify({query, variables: {input}});
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);

        assert.strictEqual(200, res.status);
        this.res = await res.json();
    } catch (e) {
        console.log("Error in Update bank account = ", e)
    }
});

Then('I should have a valid update bank account response', function() {
    assert.ok(Number.isInteger(parseInt(this.res.data.upsertCardPointePaymentBankAccount.profileId, 10)));
    assert.ok(Number.isInteger(parseInt(this.res.data.upsertCardPointePaymentBankAccount.accountId, 10)));
    assert.ok(this.res.data.upsertCardPointePaymentBankAccount.accountId === this.bankAccountId);
    assert.ok(this.res.data.upsertCardPointePaymentBankAccount.profileId === this.bankAccountProfileId);
});

