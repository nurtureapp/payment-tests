const {After} = require('@cucumber/cucumber');
const {getEnvironmentVariable} = require("../../utils/getEnvironmentVariable");
const {requestOptions} = require("../../utils/requestOptions");

After({timeout: 10000}, async function () {
    if (this.profileId && this.accountId && this.accountId_2) {
        if (this.account_2_isDefault) {
            await deleteAccount(this.profileId, this.accountId, true)
            this.accountId = null
        } else {
            await deleteAccount(this.profileId, this.accountId_2, true)
            this.accountId_2 = null
        }
    }

    if (this.profileId && this.accountId) {
        await deleteAccount(this.profileId, this.accountId, true)
    }
    if (this.profileId && this.accountId_2) {
        await deleteAccount(this.profileId, this.accountId_2, true)
    }

    if (this.bankAccountProfileId && this.bankAccountId && this.bankAccountId_2) {
        if (this.account_2_isDefault) {
            await deleteAccount(this.bankAccountProfileId, this.bankAccountId, false)
            this.bankAccountId = null
        } else {
            await deleteAccount(this.bankAccountProfileId, this.bankAccountId_2, false)
            this.bankAccountId_2 = null
        }
    }

    if (this.bankAccountProfileId && this.bankAccountId) {
        await deleteAccount(this.bankAccountProfileId, this.bankAccountId, false)
    }

    if (this.bankAccountProfileId && this.bankAccountId_2) {
        await deleteAccount(this.bankAccountProfileId, this.bankAccountId_2, false)
    }
});

async function deleteAccount(profileId, accountId, isCreditCard) {
    // let type = 'BANK'
    // if (isCreditCard) {
    //     type = 'CREDIT CARD'
    // }
    // console.log(`Delete ${type} account with profileId = ${profileId}, and accountId = ${accountId}`)

    const GRAPHQL_URL = getEnvironmentVariable("GRAPHQL_URL");
    const query = `mutation deleteCardPointePaymentAccount($input: DeleteCardPointePaymentAccountInput!) {
      deleteCardPointePaymentAccount(inputData: $input)
    }`;

    const input = {profileId, accountId};
    const body = JSON.stringify({query, variables: {input}});
    const option = await requestOptions("POST", body);
    let res = await fetch(GRAPHQL_URL, option)
    res = await res.json()
    // const data = res.data && res.data.deleteCardPointePaymentAccount ? 'SUCCESSFUL' : res
    // console.log(`Result: ${JSON.stringify(data, null, 2)}`)
    return res
}
