const {requestOptions} = require("../../utils/requestOptions");
const {getEnvironmentVariable} = require("../../utils/getEnvironmentVariable");
const assert = require('assert');
const fetch = require('node-fetch');
const {When, Then} = require('@cucumber/cucumber');
const GRAPHQL_URL = getEnvironmentVariable("GRAPHQL_URL");

When('I pay with default method', async function() {
    const schoolId = getEnvironmentVariable("SCHOOL_ID");
    const accountId = getEnvironmentVariable("ACCOUNT_ID");

    const input = {
        schoolId,
        accountId,
        profileId: this.profileId || this.bankAccountProfileId,
        amount: "10.00",
        currency: "USD"
    };

    const query = `mutation makeCardPointePayment($input: MakeCardPointePaymentInput!) {
      makeCardPointePayment(inputData: $input)
    }`;

    const body = JSON.stringify({query, variables: {input}});

    try {
      const option = await requestOptions("POST", body);
      const res = await fetch(GRAPHQL_URL, option);
      assert.strictEqual(200, res.status);

      this.res = await res.json();
    } catch (e) {
      console.log("Error in Pay by default method = ", e)
    }
});

Then('I should have a correct default payment response', function() {
    assert.ok(this.res.data.makeCardPointePayment);
});

Then('I should have a correct payment response', function() {
    assert.ok(this.res.data.makeCardPointePayment);
});


When('I pay with specific method', async function() {
    const accountId = getEnvironmentVariable("ACCOUNT_ID");

    const input = {
        accountId,
        profileId: this.profileId,
        cardpointeAccountId: this.accountId,
        schoolId: this.schoolId,
        amount: "0.10",
        currency: "USD"
    };
    const query = `mutation makeCardPointePayment($input: MakeCardPointePaymentInput!) {
      makeCardPointePayment(inputData: $input)
    }`;

    const body = JSON.stringify({query, variables: {input}});

    try {
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);
        assert.strictEqual(200, res.status);

        this.res = await res.json();
    } catch (e) {
        console.log("Error in Pay with specified method = ", e)
    }
});
