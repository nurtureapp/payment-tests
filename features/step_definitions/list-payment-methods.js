const {requestOptions} = require("../../utils/requestOptions");
const {getEnvironmentVariable} = require("../../utils/getEnvironmentVariable");
const assert = require('assert');
const fetch = require('node-fetch');
const {Given, When, Then} = require('@cucumber/cucumber');

Given('As a Parent', function () {
    // Write code here that turns the phrase above into concrete actions
});

When('I get Payment list', async function () {
    const schoolId = getEnvironmentVariable("SCHOOL_ID");
    const GRAPHQL_URL = getEnvironmentVariable("GRAPHQL_URL");
    const input = {
        schoolId
    };
    const query = `query listCardPointePaymentAccounts($input: ListCardpointeAccountInputData!) {
      listCardPointePaymentAccounts(inputData: $input) {
       items {
        accountType
        accountId
        profileId
        isDefault
        address
        address2
        bankName
        city
        country
        expiry
        isBankAccount
        name
        postal
        region
        token
       }
      }
  }`;
    const body = JSON.stringify({query, variables: {input}});
    try {
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);

        assert.strictEqual(200, res.status);
        this.res = await res.json();

    } catch (e) {
        console.error(e)
        throw new Error(e)
    }
});

Then('I should have a correct response', function () {
    assert.ok(this.res.data.listCardPointePaymentAccounts.items.length >= 1)
});



