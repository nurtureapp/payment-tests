const {requestOptions} = require("../../utils/requestOptions");
const {getEnvironmentVariable} = require("../../utils/getEnvironmentVariable");
const assert = require('assert');
const fetch = require('node-fetch');
const {When, Then} = require('@cucumber/cucumber');
const faker = require('faker');
const GRAPHQL_URL = getEnvironmentVariable("GRAPHQL_URL");

When('I subscribe to auto payment', async function () {
    const schoolId = getEnvironmentVariable("SCHOOL_ID");
    const accountId = getEnvironmentVariable("ACCOUNT_ID");

    const input = {
        schoolId,
        accountId,
        isAutoPaymentEnabled: true,
        disclaimerText: faker.random.words(),
    };
    const query = `mutation subscribeToAutoPayment($input: AutoPaymentInputData!) {
      subscribeToAutoPayment(inputData: $input)
    }`;

    try {
        const body = JSON.stringify({query, variables: {input}});
        const option = await requestOptions("POST", body);
        const res = await fetch(GRAPHQL_URL, option);

        assert.strictEqual(200, res.status);
        this.res = await res.json();
    } catch (e) {
        console.log("Error in subscribe to auto payment = ", e)
    }
});

Then('I should be subscribed successfully', async function() {
    assert.ok(this.res.data.subscribeToAutoPayment);
});
