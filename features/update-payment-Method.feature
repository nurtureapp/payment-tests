Feature: Update payment method

  Scenario: Update a credit card
    Given As a Parent
    And I create profile and add a credit card method
    When I update a credit card method
    Then I should have a valid update creadit card response

  Scenario: Update a bank account
    Given As a Parent
    And I create profile and add bank account
    When I update a bank account method
    Then I should have a valid update bank account response
