Feature: Make payment

  Scenario: Make payment with default method by credit card
    Given As a Parent
    And I create profile and add a credit card method
    When I pay with default method
    Then I should have a correct default payment response

  Scenario: Make payment with specified method by credit card
    Given As a Parent
    And I create profile and add a credit card method
    When I pay with specific method
    Then I should have a correct payment response

  Scenario: Make payment with default method by bank account
    Given As a Parent
    And I create profile and add bank account
    When I pay with default method
    Then I should have a correct payment response
