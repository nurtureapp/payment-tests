Feature: Add payment method

  Scenario: Create profile and add a credit card
    Given As a Parent
    When I create profile and add a credit card method
    Then I should have a valid create account response

  Scenario: Add a credit card to the existing profile
    Given As a Parent
    And I create profile and add a credit card method
    When I add a credit card method to existing profile
    Then I should have a valid create account response

  Scenario: Create profile and add bank account
    Given As a Parent
    When I create profile and add bank account
    Then I should have a valid create bank account response

  Scenario: Add a bank account to the existing profile
    Given As a Parent
    When I create profile and add bank account
    And I add a bank account to existing profile
    Then I should have a valid create bank account response
