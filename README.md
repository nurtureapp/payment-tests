## BEFORE RUNNING TEST
Set up env variable:

1. Specify GRAPHQL_URL, you can find it in the SETTINGS section of AppSync settings
2. Create user in nurture system
3. Add user to account
4. Specify school id
5. Specify account id - it's account where your user was added in nurture system.
6. To generate tokens use frontend (need only if test on prod env, for testing on the test env use values from .env.example file)

More details about test data you can find on the [official cardpointe site ](https://developer.cardpointe.com/guides/cardpointe-gateway)
